import requests
import json
from flask import Flask, jsonify, request


def request_headers():
    url = "https://auth.riotgames.com/api/v1/authorization"

    payload="{\r\n   \"client_id\":\"play-valorant-web-prod\",\r\n   \"nonce\":\"1\",\r\n   \"redirect_uri\":\"https://playvalorant.com/\",\r\n   \"response_type\":\"token id_token\",\r\n   \"scope\":\"account openid\",\r\n   \"withCredentials\": true\r\n}"
    headers = {
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    #response.headers.add("Access-Control-Allow-Origin", "*")
    return str(response.headers)


def authenticate(asid, username, password):
    url = "https://auth.riotgames.com/api/v1/authorization"
    payload="{\"type\":\"auth\",\"username\": \""+ username + "\", \"password\":\"" + password + "\"}"
    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'asid=' + asid + '; Path=/; Domain=auth.riotgames.com; Secure; HttpOnly;'
    }
    response = requests.request("PUT", url, headers=headers, data=payload)
    return str(response.text)

app = Flask(__name__)


@app.route('/')
def index():
    res = jsonify(request_headers())
    res.headers.add("Access-Control-Allow-Origin", "*")
    return res

@app.route('/authenticate', methods=['POST'])
def authenticate_page():
    res = jsonify(authenticate(asid = request.json['asid'], username = request.json['username'], password = request.json['password']))
    res.headers.add("Access-Control-Allow-Origin", "*")
    return res


if __name__ == '__main__':
    from waitress import serve
    serve(app, host='0.0.0.0', port=3600)
